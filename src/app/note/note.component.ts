import { NoteService } from './../services/note.service';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Note } from '../models/note';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit, OnChanges {
  @Input() selectedCategoryId: string;
  notes: Note[];

  constructor(private noteService: NoteService) {}

  ngOnInit() {
    //this.notes = this.noteService.getNotes();
    this.noteService.getNotes().subscribe((result) => {
      this.notes = result;
    })
  }

  ngOnChanges() {
    if (this.selectedCategoryId) {
      this.noteService.getFilteredNotes(this.selectedCategoryId).subscribe((result) => {
        this.notes = result;
      })
      //this.notes = this.noteService.getFilteredNotes(this.selectedCategoryId);
    }
  }
}
